<?php
class PerusahaanModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->database('default','true');
    }
	
	public function getAll()
	{
		$sql = "SELECT * FROM `prestasi`";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getById($data)
	{
		$sql = "SELECT * FROM `prestasi` WHERE IDPRESTASI = '".$data['IDPREASTASI']."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function insertAll($data)
	{
		$sql = "INSERT INTO `prestasi`(`IDPRESTASI`, `NRP`, `NAMALOMBA`, `PERINGKAT`, `THNPRESTASI`) VALUES ('','".$data['NRP']."','".$data['NAMALOMBA']."','".$data['PERINGKAT']."','"$data['PRESTASI']"')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function updateById($data)
	{
		$sql = "UPDATE `prestasi` SET `NRP`='".$data['NRP']."',`NAMALOMBA`='".$data['NAMALOMBA']."',`PERINGKAT`='".$data['PERINGKAT']."',`THNPRESTASI`='"$data['PRESTASI']"' WHERE IDPRESTASI = '".$data['IDPRESTASI']."'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function deleteById($data)
	{
		$sql = "DELETE FROM `prestasi` WHERE IDPRESTASI='".$data['IDPRESTASI']."'";
		$query = $this->db->query($sql);
		return $query;
	}
}
?>