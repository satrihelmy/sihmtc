<?php
class PerusahaanModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->database('default','true');
    }
	
	public function getAll()
	{
		$sql = "SELECT * FROM PERUSAHAAN";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getById($data)
	{
		$sql = "SELECT * FROM PERUSAHAAN WHERE IDPERUSAHAAN = '".$data['IDPERUSAHAAN']."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function insertAll($data)
	{
		$sql = "INSERT INTO `perusahaan`('', `NAMAPERUSAHAAN`, `JENISPERUSAHAAN`, `ALAMATPERUSAHAAN`, `NOTELPPERUSAHAAN`, `EMAILPERUSAHAAN`, `LINKPROPOSALKP`) VALUES ('','".$data['NAMA']."','".$data['JENIS']."','".$data['ALAMAT']."','".$data['TELP']."','".$data['EMAIL']."','".$data['LINK']."')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function updateById($data)
	{
		$sql = "UPDATE `perusahaan` SET `NAMAPERUSAHAAN`='".$data['NAMA']."',`JENISPERUSAHAAN`='".$data['JENIS']."',`ALAMATPERUSAHAAN`='".$data['ALAMAT']."',`NOTELPPERUSAHAAN`='".$data['TELP']."',`EMAILPERUSAHAAN`='".$data['EMAIL']."',`LINKPROPOSALKP`='".$data['LINK']."' WHERE IDPERUSAHAAN = '".$data['IDPERUSAHAAN']."'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function deleteById($data)
	{
		$sql = "DELETE FROM PERUSAHAAN WHERE IDPERUSAHAAN='".$data['IDPERUSAHAAN']."'";
		$query = $this->db->query($sql);
		return $query;
	}
}
?>
