<?php
class MahasiswaModel extends CI_Model
{
    function __construct() {
        parent::__construct();
    }
	
	public function getAll()
	{
		$this->load->database();
		$sql = "SELECT * FROM MAHASISWA";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getByNrp($id)
	{
		$this->load->database();
		$sql = "SELECT * FROM MAHASISWA WHERE `NRP`='".$id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function deleteByNrp($id)
	{
		$this->load->database();
		$sql = "DELETE FROM MAHASISWA WHERE `NRP`='".$id."'";
		$query = $this->db->query($sql);
		//return $query->result_array();
	}
}
?>
