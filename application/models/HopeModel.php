<?php
class HopeModel extends CI_Model
{
    public function __construct() {
        parent::__construct();
		$this->load->database('default','true');
    }
	
	public function getAll()
	{
		$sql = "SELECT * FROM HOPE";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getById($data)
	{
		$sql = "SELECT * FROM HOPE WHERE IDPROYEK = '".$data['IDPROYEK']."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function insertAll($data)
	{
		$sql = "INSERT INTO `hope`(`IDPROYEK`, `NAMAPROYEK`, `ASALPROYEK`, `JENISPROYEK`, `NAMAPROJECTMANAGER`, `NOMORTELEPONPM`, `PROYEKAVAILABLE`, `TOTALDANA`, `UANGMUKA`, `DEADLINE`, `CONTACTPERSON`, `KETERANGAN`) VALUES ('','".$data['NAMA']."','".$data['ASAL']."','".$data['JENIS']."','".$data['NAMAPM']."','".$data['NOTELPPM']."','".$data['AVAILABLE']."','".$data['TOTALDANA']."','".$data['UANGMUKA']."','".$data['DEADLINE']."','".$data['CP']."','".$data['KETERANGAN']."')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function updateById($data)
	{
		$sql = "UPDATE `hope` SET NAMAPROYEK`='".$data['NAMA']."',`ASALPROYEK`='".$data['ASAL']."',`JENISPROYEK`='".$data['JENIS']."',`NAMAPROJECTMANAGER`='".$data['NAMAPM']."',`NOMORTELEPONPM`='".$data['NOTELPPM']."',`PROYEKAVAILABLE`='".$data['AVAILABLE']."',`TOTALDANA`='".$data['TOTALDANA']."',`UANGMUKA`='".$data['UANGMUKA']."',`DEADLINE`='".$data['DEADLINE']."',`CONTACTPERSON`='".$data['CP']."',`KETERANGAN`='".$data['KETERANGAN']."' WHERE IDPROYEK = '".$data['IDPROYEK']."'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	public function deleteById($data)
	{
		$sql = "DELETE FROM HOPE WHERE IDPROYEK='".$data['IDPROYEK']."'";
		$query = $this->db->query($sql);
		return $query;
	}
}
?>
