<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>resources/img/icon/favicon.png" type="image/png">
	<title>HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/jquery/jquery-ui.css"/></head> 
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="#">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="/information">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="resources/img/web/profile/kesma.png" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information/pengpro">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#perusahaan">
						<i class="fa fa-bar-chart-o"></i> Perusahaan <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="perusahaan">
						<li>
							<a href="/information/pengpro/view">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
						<li>
							<a href="/information/pengpro/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Himpunan Mahasiswa Teknik Computer-Informatika <small>Institut Teknologi Sepuluh Nopember</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/pengpro">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Selamat Datang di Sistem Informasi HMTC ITS - Departemen Pengembangan Profesi</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						Ini adalah halaman Departemen Pengembangan Profesi
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>