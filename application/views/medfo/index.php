<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>Medfo | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>medfo">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>medfo">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->

				
				<!-- begin PUBLIKASI DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#publikasi">
						<i class="fa fa-bar-chart-o"></i> Publikasi <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="publikasi">
						<li>
							<a href="<?php echo base_url(); ?>medfo/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>medfo/update/">
								<i class="fa fa-angle-double-right"></i> Ubah
							</a>
						</li>
					</ul>
				</li> <!-- end PUBLIKASI DROPDOWN -->
				
				
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Publikasi <small>MEDFO</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url(); ?>medfo">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Masukkan Publikasi</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
						Ini adalah halaman Departemen Media Informasi
						<!-- <form:form cssClass="form-horizontal" method="GET" modelAttribute="beasiswaBean" action="add" role="form">
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='namaBeasiswa'  path="namaBeasiswa" >Nama Beasiswa</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: Bidikmisi" id="namaBeasiswa" cssClass="form-control" path="namaBeasiswa"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='asalBeasiswa'  path="asalBeasiswa" >Asal Beasiswa</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: DIKTI" id="asalBeasiswa" cssClass="form-control" path="asalBeasiswa"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='jumlahDana'  path="jumlahDana" >Jumlah Dana</form:label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<form:input required="true" placeholder="ex: 5000000" id="jumlahDana" cssClass="form-control" path="jumlahDana"></form:input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='distribusiDana'  path="distribusiDana" >Distribusi Dana</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: per Bulan" id="distribusiDana" cssClass="form-control" path="distribusiDana"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='periode'  path="periode" >Periode</form:label>
								<div class="col-md-10">
									<form:select id="periode" cssClass="form-control" path="periode">
										<form:option disabled="true" selected="true" value="#"><dfn>Periode</dfn></form:option>
										<form:option disabled="true" value="#"><hr /></form:option>
										<c:forEach items="${yearsOfPeriode}" var="item">
											<form:option value="${item}">${item}</form:option>
										</c:forEach>
									</form:select>
									<form:input required="true" placeholder="ex: 2013 - 2014" id="distribusiDana" cssClass="form-control" path="periode"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='tanggalPendaftaran'  path="tanggalPendaftaran" >Tanggal Pendaftaran</form:label>
								<div class="col-md-10">
									<form:input required="true" id="tanggalPendaftaran" cssClass="datetimepicker form-control" path="tanggalPendaftaran"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='batasPendaftaran'  path="batasPendaftaran" >Batas Pendaftaran</form:label>
								<div class="col-md-10">
									<form:input required="true" id="batasPendaftaran" cssClass="datetimepicker form-control" path="batasPendaftaran"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='linkWebsite'  path="linkWebsite" >Tautan</form:label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">http://</span>
										<form:input required="true" placeholder="ex: http://www.hmtc.com/" id="linkWebsite" cssClass="form-control" path="linkWebsite"></form:input>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Simpan</button>
								</div>
							</div>
						</form:form>
							-->
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>