<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>KMB | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="<?php echo base_url(); ?>#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>kmb">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>kmb">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->

				
				<!-- begin PROYEK DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Proyek <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="<?php echo base_url(); ?>kmb/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>kmb/view">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end PROYEK DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Proyek <small>KMB</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/kmb">Dasbor</a></li>
						<li class="active">Lihat</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Ubah Proyek</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
						<form class="form-horizontal" method="POST" modelAttribute="hopeBean" action="../update" role="form">
							
							<div class="form-group">
								<label class='col-sm-2 control-element' for='namaProyek'  path="namaProyek" >Nama Proyek</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="ex: Website" id="namaProyek" class="form-control" path="namaProyek"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='asalProyek'  path="asalProyek" >Asal Proyek</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="ex: HMTI" id="asalProyek" class="form-control" path="asalProyek"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='jenisProyek'  path="jenisProyek" >Jenis Proyek</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="ex: Aplikasi" id="jenisProyek" class="form-control" path="jenisProyek"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='namaProjectManager'  path="namaProjectManager" >Nama PM</label>
								<div class="col-md-10">
									<input type="text" placeholder="ex: Helmy Purnomo" id="namaProjectManager" class="form-control" path="namaProjectManager"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='nomorTeleponPM'  path="nomorTeleponPM" >Nomor Telepon</label>
								<div class="col-md-10">
									<input type="text" placeholder="ex: 08981711088" id="nomorTeleponPM" class="form-control" path="nomorTeleponPM"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='proyekAvailability'  path="proyekAvailability" >Ketersediaan Proyek</label>
								<div class="col-md-10">
									<select path="proyekAvailability" class="form-control">
    									<option value="1" selected="${hopeBean.getProyekAvailability()==1 ? 'selected':''}">Tersedia</option>
    									<option value="2" selected="${hopeBean.getProyekAvailability()==2 ? 'selected':''}">Tidak Tersedia</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='totalDana'  path="totalDana" >Total Dana</label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<input type="text" required="true" placeholder="ex: 5000000" id="totalDana" class="form-control" path="totalDana"></input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='uangMuka'  path="uangMuka" >Uang Muka</label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<input type="text" required="true" placeholder="ex: 5000000" id="uangMuka" class="form-control" path="uangMuka"></input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='deadline'  path="deadline" >Deadline</label>
								<div class="col-md-10">
									<input type="text" required="true" id="deadline" class="datetimepicker form-control" path="deadline"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='contactPerson'  path="contactPerson" >Contact Person</label>
								<div class="col-md-10">
									<input type="text" placeholder="ex: 08198918981" id="contactPerson" class="form-control" path="contactPerson"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='keterangan'  path="keterangan" >Keterangan</label>
								<div class="col-md-10">
									<textarea placeholder="ex: Perlu Chatting Multiclient" id="keterangan" class="form-control" path="keterangan"></textarea>
								</div>
							</div>
							
							<hr />
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>


<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>