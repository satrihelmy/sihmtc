<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page session="false" %>
 
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<c:url value="/resources/img/icon/favicon.png" />" type="image/png">
	<title>KMB | SI HMTC</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap/bootstrap.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/font-awesome.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/ubuntu.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/opensans.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/web/style.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/web/admin.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/jquery/jquery-ui.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/morris/morris-0.4.3.min.css" />" />
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<c:url value="/" />">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="<c:url value="/kmb/#" />" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="/information/kmb/">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="/information">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<c:url value="/resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information/kmb">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Proyek <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="/information/kmb/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="/information/kmb/view/">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Proyek <small>KMB</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/kesma">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Selamat Datang</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<h4>Selamat datang di halaman KMB</h4>
						<br/>
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<c:url value="/resources/js/jquery/jquery.min.js" />" ></script>
<script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui-timepicker-addon.js" />" ></script>
<script src="<c:url value="/resources/js/web/kmb.js" />" ></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>