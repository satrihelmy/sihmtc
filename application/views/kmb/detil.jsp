<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page session="false" %>
 
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<c:url value="/resources/img/icon/favicon.png" />" type="image/png">
	<title>Lihat Detil | HMTC</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap/bootstrap.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/font-awesome.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/ubuntu.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/opensans.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/web/style.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/web/admin.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/jquery/jquery-ui.css" />" />
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<c:url value="/" />">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<ul class="nav navbar-left">
				<li>
				  <a href="<c:url value="#" />" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="/information/kmb/detil/${hope.idProyek}">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="/information/kmb">
						<c:out value="KMB"/>
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<c:url value="/resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information/kmb">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Proyek <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="/information/kmb/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="/information/kmb/view/">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Himpunan Mahasiswa Teknik Computer-Informatika <small>Institut Teknologi Sepuluh Nopember</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Proyek Teknik Informatika ITS</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<table class="table table-bordered">
							<tbody>
									<tr>
										<td width="200px">Nama Proyek</td>
										<td>${hope.namaProyek}</td>
									</tr>
									<tr>
										<td>Asal Proyek</td>
										<td>${hope.asalProyek}</td>
									</tr>
									<tr>
										<td>Jenis Proyek Proyek</td>
										<td>${hope.jenisProyek}</td>
									</tr>
									<tr>
										<td>Nama PM</td>
										<td>${hope.namaProjectManager}</td>
									</tr>
									<tr>
										<td>Nomor Telepon PM</td>
										<td>${hope.nomorTeleponPM}</td>
									</tr>
									<tr>
										<td>Ketersediaan Proyek</td>
										<td><c:set var="status" value="Tersedia" /> <c:if
												test="${hope.proyekAvailability == 2}">
												<c:set var="status" value="Tidak Tersedia" />
											</c:if> ${status}</td>
									</tr>
									<tr>
										<td>Total dana</td>
										<td>${hope.totalDana}</td>
									</tr>
									<tr>
										<td>Deadline</td>
										<td>${hope.deadline}</td>
									</tr>
									<tr>
									<td></td>
										<td>
											<div class="col-sm-offset-2 col-sm-10">
												<a href="<c:url value="/kmb/update/${hope.idProyek}"/>"><button class="btn btn-default">Ubah</button></a>
											</div>
										</td>
									</tr>
								</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<c:url value="/resources/js/jquery/jquery.min.js" />" ></script>
<script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui-timepicker-addon.js" />" ></script>
<script src="<c:url value="/resources/js/web/kmb.js" />" ></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>