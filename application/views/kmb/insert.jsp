<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page session="false" %>
 
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<c:url value="/resources/img/icon/favicon.png" />" type="image/png">
	<title>Tambah Proyek | SI HMTC</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap/bootstrap.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/font-awesome.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/ubuntu.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/fonts/opensans.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/web/style.css" />" />
	<link rel="stylesheet" href="<c:url value="/resources/css/web/admin.css" />" />
	
	<link rel="stylesheet" href="<c:url value="/resources/css/jquery/jquery-ui.css" />" />
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<c:url value="/" />">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="<c:url value="/kmb/insert/#" />" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="/information/kmb/insert">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="/information">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<c:url value="/resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information/kmb">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Proyek <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="/information/kmb/insert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="/information/kmb/view/">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Proyek <small>KMB</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/kmb">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Masukkan Proyek</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
						<form:form cssClass="form-horizontal" method="POST" modelAttribute="hopeBean" action="insert" role="form">
							
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='namaProyek'  path="namaProyek" >Nama Proyek</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: Website" id="namaProyek" cssClass="form-control" path="namaProyek"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='asalProyek'  path="asalProyek" >Asal Proyek</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: HMTI" id="asalProyek" cssClass="form-control" path="asalProyek"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='jenisProyek'  path="jenisProyek" >Jenis Proyek</form:label>
								<div class="col-md-10">
									<form:input required="true" placeholder="ex: Aplikasi" id="jenisProyek" cssClass="form-control" path="jenisProyek"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='namaProjectManager'  path="namaProjectManager" >Nama PM</form:label>
								<div class="col-md-10">
									<form:input placeholder="ex: Helmy Purnomo" id="namaProjectManager" cssClass="form-control" path="namaProjectManager"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='nomorTeleponPM'  path="nomorTeleponPM" >Nomor Telepon</form:label>
								<div class="col-md-10">
									<form:input placeholder="ex: 08981711088" id="nomorTeleponPM" cssClass="form-control" path="nomorTeleponPM"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='proyekAvailability'  path="proyekAvailability" >Ketersediaan Proyek</form:label>
								<div class="col-md-10">
									<form:select path="proyekAvailability">
    									<form:options items="${ketersediaanList}" />
									</form:select>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='totalDana'  path="totalDana" >Total Dana</form:label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<form:input required="true" placeholder="ex: 5000000" id="totalDana" cssClass="form-control" path="totalDana"></form:input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='uangMuka'  path="uangMuka" >Uang Muka</form:label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<form:input required="true" placeholder="ex: 5000000" id="uangMuka" cssClass="form-control" path="uangMuka"></form:input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='deadline'  path="deadline" >Deadline</form:label>
								<div class="col-md-10">
									<form:input required="true" id="deadline" cssClass="datetimepicker form-control" path="deadline"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='contactPerson'  path="contactPerson" >Contact Person</form:label>
								<div class="col-md-10">
									<form:input placeholder="ex: 08198918981" id="contactPerson" cssClass="form-control" path="contactPerson"></form:input>
								</div>
							</div>
							<div class="form-group">
								<form:label cssClass='col-sm-2 control-element' for='keterangan'  path="keterangan" >Keterangan</form:label>
								<div class="col-md-10">
									<form:textarea placeholder="ex: Perlu Chatting Multiclient" id="keterangan" cssClass="form-control" path="keterangan"></form:textarea>
								</div>
							</div>
							
							<hr />
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Simpan</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<c:url value="/resources/js/jquery/jquery.min.js" />" ></script>
<script src="<c:url value="/resources/js/bootstrap/bootstrap.min.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui.js" />" ></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui-timepicker-addon.js" />" ></script>
<script src="<c:url value="/resources/js/web/kmb.js" />" ></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>