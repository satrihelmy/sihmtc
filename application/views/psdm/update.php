<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>PSDM | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>psdm/all">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>psdm">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#publikasi">
						<i class="fa fa-bar-chart-o"></i> ARM <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="publikasi">
						<li>
							<a href="<?php echo base_url();?>psdm/view">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
						</li>
						<li>
							<a href="<?php echo base_url();?>psdm/all">
								<i class="fa fa-angle-double-right"></i> Lihat Semua
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
				
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Data <small>Mahasiswa</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>psdm">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->	
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Update Data Diri Mahasiswa</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
									<form class="form-horizontal" role="form" method="POST" name="updateARM" modelAttribute="updateARM_Form" action="update">
										<?php
											//echo $listLabel[0];
											foreach($listLabel as $label)
											{
												$nilai = explode("/", $label);
												foreach($detailMhs as $m)
												{
												if( $nilai[2]!='GAMBARPSIKOTES' && $nilai[2]!='WARNAPSIKOTES') {
											?>
												<div class="col-md-40 form-group">
														<label class='col-md-3 control-element control-label'>
															<?php echo $nilai[0]; ?>
														</label>
													<div class="col-md-8">
													<?php
													
													if($nilai[1]=='text')
													{
													?>
														<input type="text" id="<?php echo $nilai[2]; ?>" class="form-control" placeholder="<?php echo $nilai[2];?>"  value="<?php echo $m[$nilai[2]]?>"/>
													<?php
													}
													else if($nilai[1]=='date')
													{
													?>
														<input type="text" required="true" id="<?php echo $nilai[2]; ?>" class="datetimepicker form-control" path="<?php echo $nilai[2]; ?>" value="<?php echo $m[$nilai[2]]?>"/>
													<?php
													}
													else 
													{
														$detail = explode(",",$nilai[1]);
														if($detail[0]=='select')
														{
													?>
															<select path="<?php echo $nilai[2]; ?>" class="form-control">
													<?php
															if($detail[1]=='jk')
															{
																	if($m[$nilai[2]]=='L'){
													?>
																		<option value='L' selected>Laki-Laki</option>
																	<?php } else { ?>
																		<option value='L'>Laki-Laki</option>
																	<?php } if($m[$nilai[2]]=='L'){ ?>
																		<option value='P' selected>Perempuan</option>
																	<?php } else { ?>
																		<option value='P'>Perempuan</option>
																	<?php } ?>
															<?php
															}
															else if($detail[1]=='agm')
															{
															?>
																	<?php if($m[$nilai[2]]=='Islam') { ?>
																		<option value='Islam' selected>Islam</option>
																	<?php } else { ?>
																		<option value='Islam'>Islam</option>
																	<?php } if($m[$nilai[2]]=='Kristen Katolik') { ?>
																		<option value='Kristen Katolik' selected>Kristen Katolik</option>
																	<?php } else { ?>
																		<option value='Kristen Katolik'>Kristen Katolik</option>
																	<?php } if($m[$nilai[2]]=='Kristen Protestan') { ?>
																		<option value='Kristen Protestan' selected>Kristen Protestan</option>
																	<?php } else { ?>
																		<option value='Kristen Protestan'>Kristen Protestan</option>
																	<?php } if($m[$nilai[2]]=='Hindu') { ?>
																		<option value='Hindu' selected>Hindu</option>
																	<?php } else { ?>
																		<option value='Hindu'>Hindu</option>
																	<?php } if($m[$nilai[2]]=='Budha') { ?>
																		<option value='Budha' selected>Budha</option>
																	<?php } else { ?>
																		<option value='Budha'>Budha</option>
																	<?php } if($m[$nilai[2]]=='Kong Hu Cu') { ?>
																		<option value='Kong Hu Cu' selected>Kong Hu Cu</option>
																	<?php } else { ?>
																		<option value='Kong Hu Cu'>Kong Hu Cu</option>
																	<?php } ?>
															<?php
															}
															else if($detail[1]=='thn')
															{
																	if($m[$nilai[2]]=='2012') { ?>
																		<option value='2012' selected>2012</option>
																	<?php } else { ?>
																		<option value='2012'>2012</option>
																	<?php } if($m[$nilai[2]]=='2011') { ?>	
																		<option value='2011' selected>2011</option>
																	<?php } else { ?>
																		<option value='2011'>2011</option>
																	<?php } if($m[$nilai[2]]=='2010') { ?>
																		<option value='2010' selected>2010</option>
																	<?php } else { ?>
																		<option value='2010'>2010</option>
																	<?php } if($m[$nilai[2]]=='2009') { ?>
																		<option value='2009' selected>2009</option>
																	<?php } else { ?>
																		<option value='2009'>2009</option>
																	<?php } if($m[$nilai[2]]=='2008') { ?>
																		<option value='2008' selected>2008</option>
																	<?php } else { ?>
																		<option value='2008'>2008</option>
																	<?php } if($m[$nilai[2]]=='2007') { ?>
																		<option value='2007' selected>2007</option>
																	<?php } else { ?>
																		<option value='2007'>2007</option>
																	<?php } if($m[$nilai[2]]=='2006') { ?>
																		<option value='2006' selected>2006</option>
																	<?php } else { ?>
																		<option value='2006'>2006</option>
																	<?php } if($m[$nilai[2]]=='2005') { ?>
																		<option value='2005' selected>2005</option>
																	<?php } else { ?>
																		<option value='2005'>2005</option>
																	<?php } if($m[$nilai[2]]=='2004') { ?>
																		<option value='2004' selected>2004</option>
																	<?php } else { ?>
																		<option value='2004'>2004</option>
																	<?php } if($m[$nilai[2]]=='2003') { ?>
																		<option value='2003' selected>2003</option>
																	<?php } else { ?>
																		<option value='2003'>2003</option>
																	<?php } ?>
																	
															<?php
															}
															else if($detail[1]=='fnc')
															{
															?>
																	<?php if($m[$nilai[2]]=='Lebih Mampu') { ?>
																		<option value='Lebih Mampu' selected>Lebih Mampu</option>
																	<?php } else { ?>
																		<option value='Lebih Mampu'>Lebih Mampu</option>
																	<?php } if($m[$nilai[2]]=='Mampu') { ?>
																		<option value='Mampu' selected>Mampu</option>
																	<?php } else { ?>
																		<option value='Mampu'>Mampu</option>
																	<?php } if($m[$nilai[2]]=='Cukup Mampu') { ?>
																		<option value='Cukup Mampu' selected>Cukup Mampu</option>
																	<?php } else { ?>
																		<option value='Cukup Mampu'>Cukup Mampu</option>
																	<?php } if($m[$nilai[2]]=='Kurang Mampu') { ?>
																		<option value='Kurang Mampu' selected>Kurang Mampu</option>
																	<?php } else { ?>
																		<option value='Kurang Mampu'>Kurang Mampu</option>
																	<?php } ?>
															<?php
															}
															?>
															</select>
													<?php
														}
													}
													?>
													</div>
												<div class="col-md-1">
													
												</div>
											 </div>
											 <br />
											 
											 <?php } else {
													if($nilai[2]=='GAMBARPSIKOTES') {
														if($m['GAMBARPSIKOTES']=='') { ?>
														<div class="row" style="background-color: silver;">
															<div class="row" align="center">
																<label><u>Pilih Gambar yang Kamu Suka</u></label>
															</div>
															<br>
															<div class="row" style="border-bottom-color: black;" align="center">
																  <div class="col-md-1" align="center"></div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/1.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_1"><b>1</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/2.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_2"><b>2</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/3.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_3"><b>3</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/4.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_4"><b>4</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/5.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_5"><b>5</b> </label>
																		</div>
																  </div>
															</div>
															<br>
															<div class="row" style="border-bottom-color: black;" align="center">
																  <div class="col-md-2" align="center"></div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/6.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_6"><b>6</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/7.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_7"><b>7</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/8.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_8"><b>8</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/9.png" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="gambar" value="gambar_9"><b>9</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center"></div>
															</div>
														</div>
														<br />
														
														<?php } else { ?>
														<div class="col-md-40 form-group">
															<label class='col-md-3 control-element control-label'>Gambar Psikotes</label>
															<div class="col-md-8">
																<input disabled type="text" id="GAMBARPSIKOTES" class="form-control" value="<?php echo $m['GAMBARPSIKOTES']?>" />	
															</div>
															<div class="col-md-1">
																
															</div>
														 </div>
														<?php }
															}
															
													if($nilai[2]=='WARNAPSIKOTES') {
														if($m['WARNAPSIKOTES']=='') { ?>
														
														<div class="row" style="background-color: silver;">
															<div class="row" align="center">
																<label><u>Pilih Warna yang Kamu Suka</u></label>
															</div>
															<br>
															<div class="row" style="border-bottom-color: black;" align="center">
																  <div class="col-md-2" align="center"></div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 1.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_1"><b>1</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 2.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_2"><b>2</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 3.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_3"><b>3</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 4.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_4"><b>4</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center"></div>
															</div>
															<br>
															<div class="row" style="border-bottom-color: black;" align="center">
																  <div class="col-md-3" align="center"></div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 5.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_5"><b>5</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 6.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_6"><b>6</b> </label>
																		</div>
																  </div>
																  <div class="col-md-2" align="center">
																		<img src="<?php echo base_url();?>resources/image/warna 7.jpg" class="img-thumbnail">
																		<div class="radio" align="center">
																		  <label> <input type="radio" name="warna" value="warna_7"><b>7</b> </label>
																		</div>
																  </div>
																  <div class="col-md-3" align="center"></div>
															</div>
														</div>
														<br />
														
														<?php } else {?>
														<div class="col-md-40 form-group">
															<label class='col-md-3 control-element control-label'>Warna Psikotes</label>
															<div class="col-md-8">
																<input disabled type="text" id="WARNAPSIKOTES" class="form-control" value="<?php echo $m['WARNAPSIKOTES']?>"/>
															</div>
															<div class="col-md-1">
																
															</div>
														 </div>
													<?php } 
														?>
										<?php
											}
										}
										}
										}
										?>
										
										<br />
										
										<br />
										<div class="row"  align="center" >
											<button class="btn btn-default" type="submit" >SIMPAN DATA</button>
										</div>
										
									</form>
			
				</div>
			</div>
		</div>
		
		<!-- begin PAGE TITLE ROW -->
		
	</div>


<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/dropdown.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/collapse.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js" />" ></script>
<script src="<?php echo base_url();?>resources/js/greensocks/src/minified/TweenMax.min.js" />" ></script>


<script type="text/javascript">
$(document).ready(function(){
	$('.datetimepicker').datetimepicker({
		dateFormat: 'dd MM yy',
	});
	
	$('#jumlahDana').val('');
	$('#tanggalLahir').val($('#tanggalSesungguhnya').val());
	$('input[name=gambar]:radio').change(
		    function(){
				$('#gambarPsikotes').val($('input[name=gambar]:checked').val());
		    });
	$('input[name=warna]:radio').change(
		    function(){
				$('#warnaPsikotes').val($('input[name=warna]:checked').val());
		    });
});
		
</script>
</body>
</html>