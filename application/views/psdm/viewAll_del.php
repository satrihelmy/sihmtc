<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>PSDM | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>psdm/all">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>psdm">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#publikasi">
						<i class="fa fa-bar-chart-o"></i> ARM <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="publikasi">
						<li>
							<a href="<?php echo base_url();?>psdm/view">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
						</li>
						<li>
							<a href="<?php echo base_url();?>psdm/all">
								<i class="fa fa-angle-double-right"></i> Lihat Semua
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
				
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Data <small>Mahasiswa</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>psdm">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->	
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Lihat Semua Data Mahasiswa</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
					
	<!-- begin PAGE TITLE ROW -->
				
	<div id="main">
		<div class="container" style="width:1000px">
			<div class="row">
				<div class="span12 padding10 well">
						<div class="table-responsive" style="overflow:overlay !important">
						<table class="table table-hover" id="tableMHS">
							<tr> 
								<td colspan=10 align="center"><b>Data Semua Mahasiswa</b>
								</td>
							</tr>
								<tr>
									<td>NRP</td>
									<td>Nama</td>
									<td>Gender</td>
									<td>Jalur Masuk</td>
									<td>Email</td>
									<td>Alamat Tinggal</td>
									<td>No.Handphone</td>
									<td></td>
									<td></td>
								</tr>								
								<?php
									foreach($mhs as $m)
									{
								?>
										<!--action="<?php //echo base_url(); ?>PSDM/hapusMhs" -->
										<tr>
											
												<td><?php echo $m['NRP']; ?></td>
												<td><?php echo $m['NAMA']; ?></td>
												<td><?php echo $m['JENISKELAMIN']; ?></td>
												<td><?php echo $m['JALURMASUK']; ?></td>
												<td><?php echo $m['EMAIL']; ?></td>
												<td><?php echo $m['ALAMAT']; ?></td>
												<td><?php echo $m['NOHP']; ?></td>
												<td></td>
												<td>
												<a href="<?php echo base_url(); ?>PSDM/delete/<?php echo $m['NRP']; ?>">
													<button type="submit" class="btn btn-default">delete</button>
												</a>
												</td>
											
										</tr>
								<?php
									}
								?>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
				</div>
			</div>
		</div>
		
		<!-- begin PAGE TITLE ROW -->
		
	</div>
	
<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.dropdown-toggle').dropdown();
		
		var tbl = document.getElementById("tableMHS");
        if (tbl != null) {
            for (var i = 0; i < tbl.rows.length; i++) {
                //for (var j = 0; j < tbl.rows[i].cells.length; j++)
                    tbl.rows[i].cells[j].onclick = function () { getval(tbl.rows[i].cells[j].value); };
            }
        }
 
        function getval(cel) {
            alert(cel.innerHTML);
        }
		
	});
</script>
</body>
</html>