<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>PSDM | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>psdm/view">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>psdm">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#publikasi">
						<i class="fa fa-bar-chart-o"></i> ARM <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="publikasi">
						<li>
							<a href="<?php echo base_url();?>psdm/view">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
						</li>
						<li>
							<a href="<?php echo base_url();?>psdm/all">
								<i class="fa fa-angle-double-right"></i> Lihat Semua
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
				
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Data <small>Mahasiswa</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>psdm">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->	
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Lihat Detail Data Diri Mahasiswa</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
		
					<!-- begin PAGE TITLE ROW -->
								
					<div id="main">
						<div class="container" style="width:500px">
							<div class="row">
								<div class="span12 padding10 well">
									<form class="form-inline" role="form" method="POST" name="cariDataMhs" modelAttribute="mhs" action="<?php echo base_url(); ?>PSDM/cariMhs">
										  <div class="col-sm-5">
											<label>Cari Berdasar NRP : </label>
										  </div>
										  <div class="col-sm-5">
											<input type="text" id="id" class="form-control" path="nrp" placeholder="NRP" name="id"></input>
										  </div>
											<button type="submit" class="btn btn-default">cari</button>
									</form>
									<br></br>
									<?php 
										if($cari==1)
										{
										if($detailMhs!=null)
										{			
									?>
											<table class="table table-hover">
												<tr> 
													<td colspan=2 align="center"><b>Data Detail Mahasiswa</b>
													</td>
												</tr>
												<?php
													foreach($detailMhs as $m)
													{
												?>
													<tr>	
														<td>NRP</td>
														<td><?php echo $m['NRP']; ?></td>
													</tr>
													
													<tr>
														<td>Nama Mahasiswa</td>
														<td><?php echo $m['NAMA']; ?></td>
													</tr>
													
													<tr>
														<td>Jenis Kelamin</td>
														<td><?php echo $m['JENISKELAMIN']; ?></td>
													</tr>
													
													<tr>
														<td>Tempat Lahir</td>
														<td><?php echo $m['TEMPATLAHIR']; ?></td>
													</tr>
													
													<tr>
														<td>Tanggal Lahir</td>
														<td><?php echo $m['TANGGALLAHIR']; ?></td>
													</tr>
													
													<tr>
														<td>Jalur Masuk</td>
														<td><?php echo $m['JALURMASUK']; ?></td>
													</tr>
													
													<tr>
														<td>Email</td>
														<td><?php echo $m['EMAIL']; ?></td>
													</tr>
													
													<tr>
														<td>Alamat Tinggal</td>
														<td><?php echo $m['ALAMAT']; ?></td>
													</tr>
													
													<tr>
														<td>Alamat Asal</td>
														<td><?php echo $m['ALAMATASAL']; ?></td>
													</tr>
													
													<tr>
														<td>Nomor Handphone</td>
														<td><?php echo $m['NOHP']; ?></td>
													</tr>
													
													<tr>
														<td>Agama</td>
														<td><?php echo $m['AGAMA']; ?></td>
													</tr>
													
													<tr>
														<td>Nama SD</td>
														<td><?php echo $m['SD']; ?></td>
													</tr>
													
													<tr>
														<td>Tahun Lulus SD</td>
														<td><?php echo $m['THNLULUSSD']; ?></td>
													</tr>
													
													<tr>
														<td>Nama SMP</td>
														<td><?php echo $m['SMP']; ?></td>
													</tr>
													
													<tr>
														<td>Tahun Lulus SMP</td>
														<td><?php echo $m['THNLULUSSMP']; ?></td>
													</tr>
													
													<tr>
														<td>Nama SMA</td>
														<td><?php echo $m['SMA']; ?></td>
													</tr>
													
													<tr>
														<td>Tahun Lulus SMA</td>
														<td><?php echo $m['THNLULUSSMA']; ?></td>
													</tr>
													
													<tr>
														<td>Biaya Tempat Tinggal</td>
														<td><?php echo $m['BIAYATINGGAL']; ?></td>
													</tr>
													
													<tr>
														<td>Biaya Kebutuhan Hidup</td>
														<td><?php echo $m['BIAYAHIDUP']; ?></td>
													</tr>
													
													<tr>
														<td>Biaya SPP</td>
														<td><?php echo $m['BIAYASPP']; ?></td>
													</tr>
													
													<tr>
														<td>Biaya SPI</td>
														<td><?php echo $m['BIAYASPI']; ?></td>
													</tr>
													
													<tr>
														<td>Nama Ayah</td>
														<td><?php echo $m['NAMAAYAH']; ?></td>
													</tr>
													
													<tr>
														<td>Kerja Ayah</td>
														<td><?php echo $m['KERJAAYAH']; ?></td>
													</tr>
													
													<tr>
														<td>Penghasilan Ayah</td>
														<td><?php echo $m['PENGHASILANAYAH']; ?></td>
													</tr>
													
													<tr>
														<td>Nama Ibu</td>
														<td><?php echo $m['NAMAIBU']; ?></td>
													</tr>
													
													<tr>
														<td>Kerja Ibu</td>
														<td><?php echo $m['KERJAIBU']; ?></td>
													</tr>
													
													<tr>
														<td>Penghasilan Ibu</td>
														<td><?php echo $m['PENGHASILANIBU']; ?></td>
													</tr>
													
													<tr>
														<td>Nama Wali</td>
														<td><?php echo $m['NAMAWALI']; ?></td>
													</tr>
													
													<tr>
														<td>Kerja Wali</td>
														<td><?php echo $m['KERJAWALI']; ?></td>
													</tr>
													
													<tr>
														<td>Penghasilan Wali</td>
														<td><?php echo $m['PENGHASILANWALI']; ?></td>
													</tr>
													
													<tr>
														<td>Jumlah Saudara</td>
														<td><?php echo $m['JMLSAUDARA']; ?></td>
													</tr>
													
													<tr>
														<td>Penanggung Kuliah</td>
														<td><?php echo $m['PENANGGUNGKULIAH']; ?></td>
													</tr>
													
													<tr>
														<td>Kondisi Finansial</td>
														<td><?php echo $m['KONDISIFINANSIAL']; ?></td>
													</tr>
													
													<tr>
														<!--
														<c:if test="${LabelName.split('/')[2]=='gambarPsikotes'}">
															<br/><img src="<c:url value="/resources/image/"/>${LabelName.split('/')[3].split('_')[1]}.png"/>
														</c:if>
														<c:if test="${LabelName.split('/')[2]=='warnaPsikotes'}">
															<br/><img src="<c:url value="/resources/image/warna "/>${LabelName.split('/')[3].split('_')[1]}.jpg"/>
														</c:if>
														</td>
														-->
													
											</table>
										<?php	
											}
										}
										else
										{
											?>
													<label><font color="red" size="5">Data Mahasiswa Tidak Ditemukan</font></label>
									<?php
										}
										}
									?>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		
		</div> <!-- end PAGE TITLE ROW -->
	</div>
</div>	
	

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>