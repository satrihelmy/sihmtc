<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>resources/img/icon/favicon.png" type="image/png">
	<title>HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="#">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="/information/profil">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Beasiswa <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="/information/beasiswa">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#karya">
						<i class="fa fa-bar-chart-o"></i> Karya <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="karya">
						<li>
							<a href="/information/karya">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#proyek">
						<i class="fa fa-bar-chart-o"></i> Proyek <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="proyek">
						<li>
							<a href="/information/proyek">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#publikasi">
						<i class="fa fa-bar-chart-o"></i> Publikasi <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="publikasi">
						<li>
							<a href="/information/publikasi">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#alumni">
						<i class="fa fa-bar-chart-o"></i> Alumni <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="alumni">
						<li>
							<a href="/information/alumni">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#perusahaan">
						<i class="fa fa-bar-chart-o"></i> Perusahaan <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="perusahaan">
						<li>
							<a href="/information/perusahaan">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Himpunan Mahasiswa Teknik Computer-Informatika <small>Institut Teknologi Sepuluh Nopember</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/profil">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Profil Anda</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<form:form method="POST" class="form-horizontal padding20" modelAttribute="mahasiswaBean" commandName="mhsUser" action="/information/profil/editProfil">
							<div class="form-group">
    							<label for="nrp" class="col-sm-2 control-element">NRP</label>
    							<div class="col-sm-10">
									<form:input type="text" class="form-control" placeholder="NRP" path="nrp" required="true" readonly="true" value="${mhsUser.nrp}"></form:input>
									<form:input type="hidden" id="nrp" class="form-control" path="nrp" value="${mhsUser.nrp}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="nama" class="col-sm-2 control-element">Nama</label>
								<div class="col-sm-10">
									<form:input type="text" id="nama" class="form-control" placeholder="Nama" path="nama" required="true" disabled="false" value="${mhsUser.nama}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="jenisKelamin" class="col-sm-2 control-element">Jenis Kelamin</label>
								<div class="col-sm-10">
									<form:select path="jenisKelamin"  class="form-control" items="${jk}" ></form:select>
								</div>
							</div>
							<div class="form-group">
								<label for="tanggalLahir" class="col-sm-2 control-element">Tanggal Lahir</label>
								<div class="col-sm-10">
									<form:input type="text" id="tanggalLahir" class="datepicker form-control" placeholder="tanggalLahir" path="tanggalLahir" required="true" disabled="false" value="${dateFormat.format(mhsUser.tanggalLahir)}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="jalurMasuk" class="col-sm-2 control-element">Jalur Masuk</label>
								<div class="col-sm-10">
									<form:select path="jalurMasuk" class="form-control" items="${jalurMasuk}"></form:select>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-2 control-element">Email</label>
								<div class="col-sm-10">
									<form:input type="text" id="email" class="form-control" placeholder="Email" path="email" required="true" disabled="false" value="${mhsUser.email}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="alamat" class="col-sm-2 control-element">Alamat</label>
								<div class="col-sm-10">
									<form:input type="text" id="alamat" class="form-control" placeholder="Alamat" path="alamat" required="true" disabled="false" value="${mhsUser.alamat}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="alamatAsal" class="col-sm-2 control-element">Alamat Asal</label>
								<div class="col-sm-10">
									<form:input type="text" id="alamatAsal" class="form-control" placeholder="Alamat Asal" path="alamatAsal" required="true" disabled="false" value="${mhsUser.alamatAsal}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="noHp" class="col-sm-2 control-element">Nomor Handphone</label>
								<div class="col-sm-10">
									<form:input type="text" id="noHp" class="form-control" placeholder="Nomor Handphone" path="noHp" required="true" disabled="false" value="${mhsUser.noHp}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="agama" class="col-sm-2 control-element">Agama</label>
								<div class="col-sm-10">
									<form:input type="text" id="agama" class="form-control" placeholder="Agama" path="agama" required="true" disabled="false" value="${mhsUser.agama}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="sd" class="col-sm-2 control-element">SD</label>
								<div class="col-sm-10">
									<form:input type="text" id="sd" class="form-control" placeholder="SD" path="sd" required="true" disabled="false" value="${mhsUser.sd}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="thnlulussd" class="col-sm-2 control-element">Tahun Lulus SD</label>
								<div class="col-sm-10">
									<form:input type="text" id="thnlulussd" class="form-control" placeholder="Tahun Lulus SD" path="thnLulusSd" required="true" disabled="false" value="${mhsUser.thnLulusSd}"></form:input><br/>
								</div>
							</div>
							<div class="form-group">
								<label for="smp" class="col-sm-2 control-element">SMP</label>
								<div class="col-sm-10">
									<form:input type="text" id="smp" class="form-control" placeholder="SMP" path="smp" required="true" disabled="false" value="${mhsUser.smp}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="thnlulusmp" class="col-sm-2 control-element">Tahun Lulus SMP</label>
								<div class="col-sm-10">
									<form:input type="text" id="thnlulusmp" class="form-control" placeholder="Tahun Lulus SMP" path="thnLulusSmp" required="true" disabled="false" value="${mhsUser.thnLulusSmp}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="sma" class="col-sm-2 control-element">SMA</label>
								<div class="col-sm-10">
									<form:input type="text" id="sma" class="form-control" placeholder="SMA" path="sma" required="true" disabled="false" value="${mhsUser.sma}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="thnlulusma" class="col-sm-2 control-element">Tahun Lulus SMA</label>
								<div class="col-sm-10">
									<form:input type="text" id="thnlulusmsa" class="form-control" placeholder="Tahun Lulus SMA" path="thnLulusSma" required="true" disabled="false" value="${mhsUser.thnLulusSma}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="biayatinggal" class="col-sm-2 control-element">Biaya Tinggal</label>
								<div class="col-sm-10">
									<form:input type="text" id="biayatinggal" class="form-control" placeholder="Biaya Tinggal" path="biayaTinggal" required="true" disabled="false" value="${mhsUser.biayaTinggal}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="biayahidup" class="col-sm-2 control-element">Biaya Hidup</label>
								<div class="col-sm-10">
									<form:input type="text" id="biayahidup" class="form-control" placeholder="Biaya Hidup" path="biayaHidup" required="true" disabled="false" value="${mhsUser.biayaHidup}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="biayaspp" class="col-sm-2 control-element">Biaya SPP</label>
								<div class="col-sm-10">
									<form:input type="text" id="biayaspp" class="form-control" placeholder="Biaya SPP" path="biayaSPP" required="true" disabled="false" value="${mhsUser.biayaSPP}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="biayaspi" class="col-sm-2 control-element">Biaya SPI</label>
								<div class="col-sm-10">
									<form:input type="text" id="biayaspi" class="form-control" placeholder="Biaya SPI" path="biayaSPI" required="true" disabled="false" value="${mhsUser.biayaSPI}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="namaayah" class="col-sm-2 control-element">Nama Ayah</label>
								<div class="col-sm-10">
									<form:input type="text" id="namaayah" class="form-control" placeholder="Nama Ayah" path="namaAyah" required="true" disabled="false" value="${mhsUser.namaAyah}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="namaibu" class="col-sm-2 control-element">Nama Ibu</label>
								<div class="col-sm-10">
									<form:input type="text" id="namaibu" class="form-control" placeholder="Nama Ibu" path="namaIbu" required="true" disabled="false" value="${mhsUser.namaIbu}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="kerjaayah" class="col-sm-2 control-element">Pekerjaan Ayah</label>
								<div class="col-sm-10">
									<form:input type="text" id="kerjaayah" class="form-control" placeholder="Pekerjaan Ayah" path="kerjaAyah" required="true" disabled="false" value="${mhsUser.kerjaAyah}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="kerjaibu" class="col-sm-2 control-element">Pekerjaan Ibu</label>
								<div class="col-sm-10">
									<form:input type="text" id="kerjaibu" class="form-control" placeholder="Pekerjaan Ibu" path="kerjaIbu" required="true" disabled="false" value="${mhsUser.kerjaIbu}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="penghasilanayah" class="col-sm-2 control-element">Penghasilan Ayah</label>
								<div class="col-sm-10">
									<form:input type="text" id="penghasilanayah" class="form-control" placeholder="Penghasilan Ayah" path="penghasilanAyah" required="true" disabled="false" value="${mhsUser.penghasilanAyah}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="penghasilanibu" class="col-sm-2 control-element">Penghasilan Ibu</label>
								<div class="col-sm-10">
									<form:input type="text" id="penghasilanibu" class="form-control" placeholder="Penghasilan Ibu" path="penghasilanIbu" required="true" disabled="false" value="${mhsUser.penghasilanIbu}"></form:input><br/>
								</div>
							</div>
							<div class="form-group">
								<label for="namawali" class="col-sm-2 control-element">Nama Wali</label>
								<div class="col-sm-10">
									<form:input type="text" id="namawali" class="form-control" placeholder="Nama Wali" path="namaWali" required="true" disabled="false" value="${mhsUser.namaWali}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="kerjawali" class="col-sm-2 control-element">Kerja Wali</label>
								<div class="col-sm-10">
									<form:input type="text" id="kerjawali" class="form-control" placeholder="Pekerjaan Wali" path="kerjaWali" required="true" disabled="false" value="${mhsUser.kerjaWali}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="penghasilanwali" class="col-sm-2 control-element">Penghasilan Wali</label>
								<div class="col-sm-10">
									<form:input type="text" id="penghasilanwali" class="form-control" placeholder="Penghasilan Wali" path="penghasilanWali" required="true" disabled="false" value="${mhsUser.penghasilanWali}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="jmlsaudara" class="col-sm-2 control-element">Jumlah Saudara</label>
								<div class="col-sm-10">
									<form:input type="text" id="jmlsaudara" class="form-control" placeholder="Jumlah Saudara" path="jmlSaudara" required="true" disabled="false" value="${mhsUser.jmlSaudara}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="penanggungkuliah" class="col-sm-2 control-element">Penanggung Kuliah</label>
								<div class="col-sm-10">
									<form:input type="text" id="penanggungkuliah" class="form-control" placeholder="Penanggung Kuliah" path="penanggungKuliah" required="true" disabled="false" value="${mhsUser.penanggungKuliah}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="tahunlulus" class="col-sm-2 control-element">Tahun Lulus</label>
								<div class="col-sm-10">
									<form:input type="text" readonly="true" class="form-control" placeholder="Tahun Lulus" path="tahunLulus" required="true" disabled="false" value="${mhsUser.tahunLulus}"></form:input>
									<form:input type="hidden" id="tahunlulus" class="form-control" path="tahunLulus" value="${mhsUser.tahunLulus}"></form:input>
								</div>
							</div>
							<div class="form-group">
								<label for="status" class="col-sm-2 control-element">Status</label>
								<div class="col-sm-10">
									<form:input type="text" id="status" class="form-control" placeholder="Status" path="status" required="true" disabled="false" value="${mhsUser.status}"></form:input><br/>
								</div>
							</div>
							<button class="btn btn-primary btn-block" style="width: 100px" type="submit">Perbarui</button>
						</form:form>
					
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>