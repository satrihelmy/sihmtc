 <!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>Beranda | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/home.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>

<div id="main">	
	<div class="cover-img padding20">
		<div class="col-md-offset-8" style="width:300px; margin-top:80px">
			<div class="row">
				<div class="span12 padding20 shadow" style="background-color:rgb(132, 166, 177);">
					<form method="POST" name="Login" action="login">
						<h2 class="form-signin-heading fg-white">Masuk !</h2>
						<hr />
						<input type="text" id="nrp" name="nrp" class="form-control" placeholder="nrp" required="true">
						<br />
						<input type="password" class="form-control" placeholder="kata kunci" name="password" required="true">
						<br />
						<br />
						
						<button class="btn btn-default" style="border-radius:0px" type="submit">MASUK</button>
				</div>
				
				<div class="span12 padding5 shadow text-center" style="background-color:rgb(132, 166, 177); margin-top:10px; cursor:pointer">
					<h4 >
						<a href="<?php echo base_url();?>main/about">
							<span class="glyphicon glyphicon-earphone"></span> About Us !
						</a>
					</h4>
				</div>
				
			</div>	
		</div>
	</div>
	<div id="" style="border-bottom:1px #CECECE solid">
		<br /> 
		<div class="container">
			<p align="center">- Sistem Informasi Himpunan Mahasiswa Teknik Computer-Informatika : <em>Pusat Informasi Dalam Satu Portal Informasi</em> -</p>
		</div>
	</div>
</div>


<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js" ></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/dropdown.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/collapse.js"></script>
<script src="<?php echo base_url();?>resources/js/greensocks/src/minified/TweenMax.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

	});
</script>
</body>
</html>
