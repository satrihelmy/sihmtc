<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>resources/img/icon/favicon.png" type="image/png">
	<title>HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/jquery/jquery-ui.css"/></head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="#">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="/information/profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="/information/logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>resources/img/web/profile/kesma.png" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="/information/ristek">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				<!-- begin KARYA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#karya">
						<i class="fa fa-bar-chart-o"></i> Karya <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="karya">
						<li>
							<a href="/information/ristek/inputKarya/">
								<i class="fa fa-angle-double-right"></i> Memasukkan karya
							</a>
						</li>
						<li>
							<a href="/information/ristek/viewKarya">
								<i class="fa fa-angle-double-right"></i> Melihat karya
							</a>
						</li>
					</ul>
				</li> <!-- end KARYA DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Masukkan Karya <small>Anak HMTC</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="/information/ristek">Dasbor</a></li>
						<li class="active">Memasukkan Karya</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Masukkan karya Anak HMTC ITS</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
						<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>ristek/tambahKarya" role="form">
							<div class="form-group">
								<label class='col-md-2 control-element' for='namaKarya'>Nama Karya</label>
								<div class="col-md-10">
									<input type="text" placeholder="nama karya" id="namaKarya" cssClass="form-control" name="namaKarya"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class='col-md-2 control-element' for='timPembuatKarya'  name="timPembuatKarya">Tim Pembuat Karya</label>
								<div class="col-md-10">
									<input type="text" placeholder="tim pembuat" id="timPembuatKarya" cssClass="form-control" name="timPembuatKarya"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class='col-md-2 control-element' for='jenisKarya'  name="jenisKarya" >Jenis Karya</label><br><br>
								<div class="col-md-10">
								<input type="checkbox" name="jenisKarya" value="IPL" />&nbsp;&nbsp;Inovasi Perangkat Lunak <br>
									<input type="checkbox" name="jenisKarya" value="Aplikasi" />&nbsp;&nbsp;Rekayasa Perangkat Lunak <br>
									<input type="checkbox" name="jenisKarya" value="Game" />&nbsp;&nbsp;Permainan <br> 
									<input type="checkbox" name="jenisKarya" value="Sistem Cerdas" />&nbsp;&nbsp;Sistem Cerdas <br>
									<input type="checkbox" name="jenisKarya" value="Jaringan" />&nbsp;&nbsp;Jaringan Komputer <br>
								</div>
							</div>
							
							<div class="form-group">
								<label class='col-md-2 control-element' for='tahunKarya'  name="tahunKarya" >Tahun Karya</label>
								<div class="col-md-10">
									<select name="tahunKarya" class="form-control">
									<option value="2013">2013</option>
									<option value="2013">2012</option>
									<option value="2013">2011</option>
									<option value="2013">2010</option>
							    	</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class='col-md-2 control-element' for='statusKarya'  name="statusKarya" >Status Karya</label>
								<div class="col-md-10">
									<input type="text" placeholder="status karya" id="statusKarya" cssClass="form-control" name="statusKarya"></input>
								</div>
							</div>
							
							<div class="form-group">
								<label class='col-md-2 control-element' for='partisipasiLomba'  name="partisipasiLomba" >Partisipasi Lomba</label>
								<div class="col-md-10">
									<input type="textarea" placeholder="partisipasi lomba" id="partisipasiLomba" cssClass="form-control" name="partisipasiLomba" rows="4"/>
								</div>
							</div>
							
							<hr />
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Simpan</button>
								</div>
							</div>
							
							<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							        <h4 class="modal-title" id="myModalLabel">Verifikasi Data</h4>
							      </div>
							      <div class="modal-body">
							        Anda yakin dengan data yang anda masukkan?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							        <button type="submit" class="btn btn-primary">Simpan Data</button>
							      </div>
							    </div><!-- /.modal-content -->
							  </div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>