<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>Kesma | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="/#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>kesma">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>/resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>kesma">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Beasiswa <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="<?php echo base_url();?>kesma/beasiswaInsert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>kesma/beasiswaView">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
				<!-- begin EBDB DROPDOWN -->
				<li class="panel">
					<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ebdb">
						<i class="fa fa-bar-chart-o"></i> EBDB <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="ebdb">
						<li>
							<a href="<?php echo base_url();?>kesma/ebdbView">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>EBDB <small>KESMA</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>kesma">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Daftar Beasiswa</h4>
						</div>
	                    	<div class="clearfix"></div>
	                    </div>
						<div class="portlet-body">
							<br />
							<h1>
							</h1>
							<div class="table-resposive" style="overflow:overlay">
								<table class="table table-green table-bordered">
									<thead>
										<tr>
											<th>NRP</th>
											<th>Nama</th>
											<th>Jenis Kelamin</th>
											<th>Tempat Lahir</th>
											<th>Tanggal Lahir</th>
											<th>Jalur Masuk</th>
											<th>Email</th>
											<th>Alamat</th>
											<th>Alamat Asal</th>
											<th>Nomor HP</th>
											<th>Agama</th>
											<th>SD</th>
											<th>Tahun Lulus SD</th>
											<th>SMP</th>
											<th>Tahun Lulus SMP</th>
											<th>SMA</th>
											<th>Tahun Lulus SMA</th>
											<th>Biaya Tinggal</th>
											<th>Biaya Hidup</th>
											<th>Biaya SPP</th>
											<th>Biaya SPI</th>
											<th>Nama Ayah</th>
											<th>Nama Ibu</th>
											<th>Pekerjaan Ayah</th>
											<th>Pekerjaan Ibu</th>
											<th>Penghasilan Ayah</th>
											<th>Penghasilan Ibu</th>
											<th>Nama Wali</th>
											<th>Pekerjaan Wali</th>
											<th>Penghasilan Wali</th>
											<th>Jumlah Saudara</th>
											<th>Penanggung Kuliah</th>
											<th>Kondisi Finansial</th>
											<th>Gambar Psikotes</th>
											<th>Warna Psikotes</th>
											<th>Tahun Lulus</th>
											<th>Status</th>
											<th>Karya Ilmiah</th>
											<th>Prestasi</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach($mhs as $dt)
											{
											?>
											<tr>
												<td><?php echo $dt['NRP']; ?></td>
												<td><?php echo $dt['NAMA']; ?></td>
												<td><?php echo $dt['JENISKELAMIN']; ?></td>
												<td><?php echo $dt['TEMPATLAHIR']; ?></td>
												<td><?php echo $dt['TANGGALLAHIR']; ?></td>
												<td><?php echo $dt['JALURMASUK']; ?></td>
												<td><?php echo $dt['EMAIL']; ?></td>
												<td><?php echo $dt['ALAMAT']; ?></td>
												<td><?php echo $dt['ALAMATASAL']; ?></td>
												<td><?php echo $dt['NOHP']; ?></td>
												<td><?php echo $dt['AGAMA']; ?></td>
												<td><?php echo $dt['SD']; ?></td>
												<td><?php echo $dt['THNLULUSSD']; ?></td>
												<td><?php echo $dt['SMP']; ?></td>
												<td><?php echo $dt['THNLULUSSMP']; ?></td>
												<td><?php echo $dt['SMA']; ?></td>
												<td><?php echo $dt['THNLULUSSMA']; ?></td>
												<td><?php echo $dt['BIAYATINGGAL']; ?></td>
												<td><?php echo $dt['BIAYAHIDUP']; ?></td>
												<td><?php echo $dt['BIAYASPP']; ?></td>
												<td><?php echo $dt['BIAYASPI']; ?></td>
												<td><?php echo $dt['NAMAAYAH']; ?></td>
												<td><?php echo $dt['NAMAIBU']; ?></td>
												<td><?php echo $dt['KERJAAYAH']; ?></td>
												<td><?php echo $dt['KERJAIBU']; ?></td>
												<td><?php echo $dt['PENGHASILANAYAH']; ?></td>
												<td><?php echo $dt['PENGHASILANIBU']; ?></td>
												<td><?php echo $dt['NAMAWALI']; ?></td>
												<td><?php echo $dt['KERJAWALI']; ?></td>
												<td><?php echo $dt['PENGHASILANWALI']; ?></td>
												<td><?php echo $dt['JMLSAUDARA']; ?></td>
												<td><?php echo $dt['PENANGGUNGKULIAH']; ?></td>
												<td><?php echo $dt['KONDISIFINANSIAL']; ?></td>
												<td><?php echo $dt['GAMBARPSIKOTES']; ?></td>
												<td><?php echo $dt['WARNAPSIKOTES']; ?></td>
												<td><?php echo $dt['TAHUNLULUS']; ?></td>
												<td><?php echo $dt['STATUS']; ?></td>
												<td> </td>
												<td> </td>
													
													
																			
											</tr>
										<?php
											}
										?>
									</tbody>
								</table>
							</div>
							 <ul class="pagination">
								<li><a href="#">&laquo;</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  <!-- end MAIN PAGE CONTENT -->

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		
	});
</script>
</body>
</html>