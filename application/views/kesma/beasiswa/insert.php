<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url();?>resources/img/icon/favicon.png" type="image/png">
	<title>Kesma | SI HMTC</title>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/bootstrap/bootstrap.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/font-awesome.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/ubuntu.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/fonts/opensans.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/web/admin.css"/>
	
	<link rel="stylesheet" href="<?php echo base_url();?>resources/css/jquery/jquery-ui.css"/>
</head> 
<body>
<div id="wrapper">
	<!-- begin TOP NAVIGATION -->
	<nav class="navbar-top">
		
		<!-- begin BRAND HEADING -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle pull-right btn-btn-info" data-toggle="collapse" data-target=".sidebar-collapse">
				<i class="fa fa-bars"></i>
				
			</button>
			<div class="navbar-brand">
				<a href="<?php echo base_url();?>">
					Sistem Informasi HMTC
				</a>
			</div>
		</div>
		<!-- end BRAND HEADING -->

		<div class="nav-top">
			<!-- begin LEFT SIDE TOP BAR -->
			<ul class="nav navbar-left">
				<li>
				  <a href="<?php echo base_url();?>kesma/insert/#" id="sidebar-toggle">
				  	<i class="fa fa-bars"></i>
				  </a>
				</li>
				<li>
					<a href="<?php echo base_url();?>kesma">
						<i class="fa fa-refresh"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">
						Kembali ke menu utama
					</a>
				</li>
      		</ul> <!-- end LEFT SIDE TOP BAR -->
      		
      		<!-- begin RIGHT SIDE TOP BAR -->
      		<ul class="nav navbar-right">
				<li class="dropdown">
					<a class="bg-amber dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu list-unstyled">
						<li>
							<a href="<?php echo base_url();?>profil">
								<i class="fa fa-user"></i> Profil
							</a>
						</li>
          					<li class="divider"></li>
						<li>
							<a href="<?php echo base_url();?>logout">
								<i class="fa fa-power-off"></i> Keluar
							</a>
						</li>
					</ul>
				</li>
      		</ul> <!-- end RIGHT SIDE TOP BAR -->
		</div>
	</nav> <!-- end TOP NAVIGATION -->
	
	<!-- begin SIDE NAVIGATION -->
	<nav class="navbar-side" role="navigation">
		<div class="navbar-collapse sidebar-collapse collapse">
			<ul id="side" class="nav navbar-nav side-nav">
				
				<!-- begin SIDE NAV USER PANEL -->
				<li class="side-user hidden-xs">
					<img class="img-circle" width="150px" src="<?php echo base_url();?>/resources/img/web/profile/kesma.png" />" alt="">
					<p class="welcome">
						<i class="fa fa-key"></i> Masuk sebagai
					</p>
					<p class="name">
						<span class="last-name">${department}</span> HMTC
					</p>
					<div class="clearfix"></div>
				</li> <!-- end SIDE NAV USER PANEL -->
				
				<!-- begin DASHBOARD LINK -->
				<li>
					<a href="<?php echo base_url();?>kesma">
						<i class="fa fa-dashboard"></i> Dasbor
					</a>
				</li> <!-- end DASHBOARD LINK -->
				
				<!-- begin BEASISWA DROPDOWN -->
				<li class="panel">
					<a data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#beasiswa">
						<i class="fa fa-bar-chart-o"></i> Beasiswa <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="beasiswa">
						<li>
							<a href="<?php echo base_url();?>kesma/beasiswaInsert">
								<i class="fa fa-angle-double-right"></i> Baru
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>kesma/beasiswaView">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
				
				<!-- begin EBDB DROPDOWN -->
				<li class="panel">
					<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ebdb">
						<i class="fa fa-bar-chart-o"></i> EBDB <i class="fa fa-caret-down"></i>
					</a>
					<ul class="collapse nav" id="ebdb">
						<li>
							<a href="<?php echo base_url();?>kesma/ebdbView">
								<i class="fa fa-angle-double-right"></i> Lihat
							</a>
						</li>
					</ul>
				</li> <!-- end BEASISWA DROPDOWN -->
			</ul>
		</div>
	</nav> <!-- end SIDE NAVIGATION -->
	
	<!-- begin MAIN PAGE CONTENT -->
	<div id="page-wrapper">
		
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Beasiswa <small>KESMA</small></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>/kesma">Dasbor</a></li>
						<li class="active">Baru</li>
					</ol>
				</div>
			</div>
		</div>  <!-- end PAGE TITLE ROW -->
		
		<!-- begin CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Masukkan Beasiswa</h4>
						</div>
                    	<div class="clearfix"></div>
                    </div>
					<div class="portlet-body">
						<br />
						<h1>
						</h1>
						
						<form class="form-horizontal" method="POST" modelAttribute="beasiswaBean" action="<?php echo base_url(); ?>kesma/tambahBeasiswa" role="form">
							<div class="form-group">
								<label class='col-sm-2 control-element' for='namaBeasiswa'  path="namaBeasiswa" >Nama Beasiswa</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="Bidikmisi" id="namaBeasiswa" class="form-control" path="namaBeasiswa" name="nama"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='asalBeasiswa'  path="asalBeasiswa" >Asal Beasiswa</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="DIKTI" id="asalBeasiswa" class="form-control" path="asalBeasiswa" name="asal"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='jumlahDana'  path="jumlahDana" >Jumlah Dana</label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">Rp.</span>
										<input type="text" required="true" placeholder="5000000" id="jumlahDana" class="form-control" path="jumlahDana" name="jml"></input>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='distribusiDana'  path="distribusiDana" >Distribusi Dana</label>
								<div class="col-md-10">
									<input type="text" required="true" placeholder="per Bulan" id="distribusiDana" class="form-control" path="distribusiDana" name="distribusi"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='periode'  path="periode" >Periode</label>
								<div class="col-md-10">
									<select id="periode" class="form-control" path="periode" name="periode">
										<option disabled="true" selected="true" value="#"><dfn>Periode</dfn></option>
																	<option value='2012'>2013</option>
																	<option value='2012'>2012</option>
																	<option value='2011'>2011</option>
																	<option value='2010'>2010</option>
																	<option value='2009'>2009</option>
										<!--
										<option disabled="true" value="#"><hr /></option>
										
										
										<c:forEach items="${yearList}" var="year">
											<form:option value="${year}">${year}</form:option>
										</c:forEach>
										-->
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='tanggalPendaftaran'  path="tanggalPendaftaran" >Tanggal Pendaftaran</label>
								<div class="col-md-10">
									<input type="text" required="true" id="tanggalPendaftaran" class="datetimepicker form-control" path="tanggalPendaftaran" name="daftar"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='batasPendaftaran'  path="batasPendaftaran" >Batas Pendaftaran</label>
								<div class="col-md-10">
									<input type="datetime" required="true" id="batasPendaftaran" class="datetimepicker form-control" path="batasPendaftaran" name="batas"></input>
								</div>
							</div>
							<div class="form-group">
								<label class='col-sm-2 control-element' for='linkWebsite'  path="linkWebsite" >Tautan</label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">http://</span>
										<input type="text" required="true" placeholder="www.hmtc.com/" id="linkWebsite" class="form-control" path="linkWebsite" name="link"></input>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Simpan</button>
								</div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>  <!-- end MAIN PAGE CONTENT -->
</div>

<script src="<?php echo base_url();?>resources/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>resources/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui.js"></script>
<script src="<?php echo base_url();?>resources/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo base_url();?>resources/js/web/kesma.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var src = ${asalBeasiswa};
			$( "#asalBeasiswa" ).autocomplete({	
				source: src
			});
	});
</script>
</body>
</html>