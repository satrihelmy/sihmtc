<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medfo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	// constructor, buat ngeload model yang mau dipanggil nanti
	function __construct() {
        parent::__construct();
		$this->load->model('PublikasiModel');
		session_start();
    }
	public function index()
	{
		$this->load->view('medfo/index');
	}
	public function insert()
	{
		$this->load->view('medfo/insert');
	}
	public function update()
	{
		$this->load->view('medfo/update');
	}
	
	public function lihat()
	{
		// ini buat manggil fungsi yang ada di model namanya getAll();
		$data['hasil'] = $this->PublikasiModel->getAll();
		// ini buat masukin ke view data-data yang diambil dari model
		$this->load->view('medfo/view',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */