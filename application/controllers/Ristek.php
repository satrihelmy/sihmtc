<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ristek extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
		$this->load->model('KaryaModel');
		session_start();
    }
	 
	public function index()
	{
		$this->load->view('ristek/index');
	}
	
	public function insert()
	{
		$this->load->view('ristek/insert');
	}
	
	public function view()
	{
		// ini buat manggil fungsi yang ada di model namanya getAll();
		$data['karyaAll'] = $this->KaryaModel->getAll();
		// ini buat masukin ke view data-data yang diambil dari model
		$this->load->view('ristek/view',$data);
		
		//$this->load->view('ristek/view');
	}
	
	public function tambahKarya()
	{
		//$this->load->view('ristek/insert');
		$data['NAMAKARYA']=$_POST['namaKarya'];
		$data['TIMPEMBUATKARYA']=$_POST['timPembuatKarya'];
		$data['JENISKARYA']=$_POST['jenisKarya'];
		$data['TAHUNKARYA']=$_POST['tahunKarya'];
		$data['STATUSKARYA']=$_POST['statusKarya'];
		$data['PARTISIPASILOMBA']=$_POST['partisipasiLomba'];
		$this->KaryaModel->insertBeasiswa($data);
		$this->load->view('ristek/insert');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */