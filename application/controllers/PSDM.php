<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PSDM extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('MahasiswaModel');
		$labelList = array();
    }
	
	public function index()
	{
		$this->load->view('psdm/index');
	}
	public function view()
	{
		$data['cari']=0;
		$data['get']=0;
		$this->load->view('psdm/view', $data);
	}
	
	public function cariMhs()
	{
		$data['cari']=1;
		$data['detailMhs'] = $this->MahasiswaModel->getByNrp($_POST['id']);
		$this->load->view('psdm/view', $data);
	}
	public function all()
	{
		$data['mhs'] = $this->MahasiswaModel->getAll();
		
		$this->load->view('psdm/viewAll_del',$data);
	}
	

	public function delete($NRP)
	{
		$this->MahasiswaModel->deleteByNrp($NRP);
		
		redirect('psdm/all');
	}
	
	
	public function update()
	{
		
		$labelList[0] = "NRP/text/NRP/";
		$labelList[1] = "Nama Mahasiswa/text/NAMA/";
		$labelList[2] = "Jenis Kelamin/select,jk/JENISKELAMIN/";
		$labelList[3] = "Tempat Lahir/text/TEMPATLAHIR/";
		$labelList[4] = "Tanggal Lahir/date/TANGGALLAHIR/";
		$labelList[5] = "Jalur Masuk/text/JALURMASUK/";
		$labelList[6] = "Email/text/EMAIL/";
		$labelList[7] = "Alamat Tinggal/text/ALAMAT/";
		$labelList[8] = "Alamat Asal/text/ALAMATASAL/";
		$labelList[9] = "Nomor Handphone/text/NOHP/";
		$labelList[10] = "Agama/select,agm/AGAMA/";
		$labelList[11] = "Nama SD/text/SD/";
		$labelList[12] = "Tahun Lulus SD/select,thn/THNLULUSSD/";
		$labelList[13] = "Nama SMP/text/SMP/";
		$labelList[14] = "Tahun Lulus SMP/select,thn/THNLULUSSMP/";
		$labelList[15] = "Nama SMA/text/SMA/";
		$labelList[16] = "Tahun Lulus SMA/select,thn/THNLULUSSMA/";
		$labelList[17] = "Biaya Tempat Tinggal/text/BIAYATINGGAL/";
		$labelList[18] = "Biaya Kebutuhan Hidup/text/BIAYAHIDUP/";
		$labelList[19] = "Biaya SPP/text/BIAYASPP/";
		$labelList[20] = "Biaya SPI/text/BIAYASPI/";
		$labelList[21] = "Nama Ayah/text/NAMAAYAH/";
		$labelList[22] = "Kerja Ayah/text/KERJAAYAH/";
		$labelList[23] = "Penghasilan Ayah/text/PENGHASILANAYAH/";
		$labelList[24] = "Nama Ibu/text/NAMAIBU/";
		$labelList[25] = "Kerja Ibu/text/KERJAIBU/";
		$labelList[26] = "Penghasilan Ibu/text/PENGHASILANIBU/";
		$labelList[27] = "Nama Wali/text/NAMAWALI/";
		$labelList[28] = "Kerja Wali/text/KERJAWALI/";
		$labelList[29] = "Penghasilan Wali/text/PENGHASILANWALI/";
		$labelList[30] = "Jumlah Saudara/text/JMLSAUDARA/";
		$labelList[31] = "Penanggung Kuliah/text/PENANGGUNGKULIAH/";
		$labelList[32] = "Kondisi Finansial/select,fnc/KONDISIFINANSIAL/";
		$labelList[33] = "Gambar Psikotes/text/GAMBARPSIKOTES/";
		$labelList[34] = "Warna Psikotes/text/WARNAPSIKOTES/";
		
		$data['detailMhs'] = $this->MahasiswaModel->getByNrp('5111100001');
		$data['listLabel'] = $labelList;
		$this->load->view('psdm/update', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */