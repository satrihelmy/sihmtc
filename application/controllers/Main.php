<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('home');
	}
	
	public function about()
	{
		$this->load->view('about');
	}
	
	public function alumni()
	{
		$this->load->view('alumni');
	}
	
	public function beasiswa()
	{
		$this->load->view('beasiswa');
	}
	
	public function karya()
	{
		$this->load->view('karya');
	}
	
	public function perusahaan()
	{
		$this->load->view('perusahaan');
	}
	
	public function profil()
	{
		$this->load->view('profil');
	}
	
	public function proyek()
	{
		$this->load->view('proyek');
	}
	
	public function publikasi()
	{
		$this->load->view('publikasi');
	}
	
	public function user()
	{
		$this->load->view('user');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */