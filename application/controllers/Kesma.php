<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesma extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('MahasiswaModel');
		$this->load->model('BeasiswaModel');
		session_start();
		$columnList = array();
    }
	
	public function index()
	{
		$this->load->view('kesma/index');
	}
	public function ebdbView()
	{
		$columnList[0] = "NRP";
		$columnList[1] = "NAMA";
		$columnList[2] = "JENISKELAMIN";
		$columnList[3] = "TEMPATLAHIR";
		$columnList[4] = "TANGGALLAHIR";
		$columnList[5] = "JALURMASUK";
		$columnList[6] = "EMAIL";
		$columnList[7] = "ALAMAT";
		$columnList[8] = "ALAMATASAL";
		$columnList[9] = "NOHP";
		$columnList[10] = "AGAMA";
		$columnList[11] = "SD";
		$columnList[12] = "THNLULUSSD";
		$columnList[13] = "SMP";
		$columnList[14] = "THNLULUSSMP";
		$columnList[15] = "SMA";
		$columnList[16] = "THNLULUSSMA";
		$columnList[17] = "BIAYATINGGAL";
		$columnList[18] = "BIAYAHIDUP";
		$columnList[19] = "BIAYASPP";
		$columnList[20] = "BIAYASPI";
		$columnList[21] = "NAMAAYAH";
		$columnList[22] = "NAMAIBU";
		$columnList[23] = "KERJAAYAH";
		$columnList[24] = "KERJAIBU";
		$columnList[25] = "PENGHASILANAYAH";
		$columnList[26] = "PENGHASILANIBU";
		$columnList[27] = "NAMAWALI";
		$columnList[28] = "KERJAWALI";
		$columnList[29] = "PENGHASILANWALI";
		$columnList[30] = "JMLSAUDARA";
		$columnList[31] = "PENANGGUNGKULIAH";
		$columnList[32] = "KONDISIFINANSIAL";
		$columnList[33] = "GAMBARPSIKOTES";
		$columnList[34] = "WARNAPSIKOTES";
		$columnList[35] = "TAHUNLULUS";
		$columnList[36] = "STATUS";
		
		//$data['column'] = $columnList;
		
		$data['mhs'] = $this->MahasiswaModel->getAll();
		
		$this->load->view('kesma/ebdb/view', $data);
	}
	public function beasiswaInsert()
	{
		$this->load->view('kesma/beasiswa/insert');
	}
	public function beasiswaView()
	{
		$data['beasiswa'] = $this->BeasiswaModel->getAll();
		$this->load->view('kesma/beasiswa/view', $data);
	}
	
	public function bulan($kata)
	{
		if($kata=="January")
		{
			return "01";
		}
		else if($kata=="February")
		{
			return "02";
		}
		else if($kata=="March")
		{
			return "03";
		}
		else if($kata=="April")
		{
			return "04";
		}
		else if($kata=="May")
		{
			return "05";
		}
		else if($kata=="June")
		{
			return "06";
		}
		else if($kata=="July")
		{
			return "07";
		}
		else if($kata=="August")
		{
			return "08";
		}
		else if($kata=="September")
		{
			return "09";
		}
		else if($kata=="October")
		{
			return "10";
		}
		else if($kata=="November")
		{
			return "11";
		}
		else if($kata=="December")
		{
			return "12";
		}
		
	}
	
	public function tambahBeasiswa()
	{
		
		$bea['NAMABEASISWA'] = $_POST['nama'];
		$bea['ASALBEASISWA'] = $_POST['asal'];
		$bea['JUMLAHDANA'] = $_POST['jml'];
		$bea['DISTRIBUSIDANA'] = $_POST['distribusi'];
		$bea['PERIODE'] = $_POST['periode'];
		
		$daftar = explode(" ", $_POST['daftar']);
		$batas = explode(" ", $_POST['batas']);
		
		$bea['TANGGALPENDAFTARAN'] = $daftar[2]."-".$this->bulan($daftar[1])."-".$daftar[0];
		$bea['BATASPENDAFTARAN'] = $batas[2]."-".$this->bulan($batas[1])."-".$batas[0];
		
		$bea['LINKWEBSITE'] = $_POST['link'];
		$this->BeasiswaModel->insertBeasiswa($bea);
		$this->load->view('kesma/beasiswa/Insert');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */