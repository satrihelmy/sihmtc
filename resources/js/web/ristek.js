//untuk mengcustom required
$('input').on('invalid', function (e) {
	this.setCustomValidity("Wajib diisi!");
});

$('.fa-refresh').click(function (e) {
    $(this).addClass("fa-spin fa-spinner");
});

// bawaannya bootstrap
$("#sidebar-toggle").click(function (e) {
	e.preventDefault();
	$(".navbar-side").toggleClass("collapsed");
	$("#page-wrapper").toggleClass("collapsed");
});