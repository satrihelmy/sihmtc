/**
 * 
 */

$('.datetimepicker').datetimepicker({
	dateFormat: 'dd MM yy',
});

$('.datepicker').datepicker({
	dateFormat: 'dd MM yy',
});

//untuk mengcustom required
$('input').on('invalid', function (e) {
	this.setCustomValidity("Wajib diisi!");
});

// untuk mengenolkan kembali inputnya
$('input').on('input', function (e) {
    this.setCustomValidity("");
});

$('.fa-refresh').click(function (e) {
    $(this).addClass("fa-spin fa-spinner");
});

// bawaannya bootstrap
$("#sidebar-toggle").click(function (e) {
	e.preventDefault();
	$(".navbar-side").toggleClass("collapsed");
	$("#page-wrapper").toggleClass("collapsed");
});