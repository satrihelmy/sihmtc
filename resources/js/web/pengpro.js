$('input').on('invalid', function (e) {
	this.setCustomValidity("Wajib diisi!");
});

$('input').on('input', function (e) {
    this.setCustomValidity("");
});

$('.fa-refresh').click(function (e) {
    $(this).addClass("fa-spin fa-spinner");
});

$("#sidebar-toggle").click(function (e) {
	e.preventDefault();
	$(".navbar-side").toggleClass("collapsed");
	$("#page-wrapper").toggleClass("collapsed");
});